class PagesController < ApplicationController
  before_action :authenticate_user!

  def home
    @campaigns = current_user.campaigns
    redirect_to '/campaigns' if @campaigns
  end
end
