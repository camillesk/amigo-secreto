require 'rails_helper'

RSpec.describe CampaignRaffleJob, type: :job do
  before(:each) do
    @current_user = FactoryBot.create(:user)
    @campaign = create(:campaign, user: @current_user)
    create(:member, campaign: @campaign)
    create(:member, campaign: @campaign)
    create(:member, campaign: @campaign)
  end

  describe "#perform" do
    it "raffles a campaign" do
      ActiveJob::Base.queue_adapter = :test
      expect {
        CampaignRaffleJob.perform_later @campaign
      }.to have_enqueued_job
    end
  end
end
