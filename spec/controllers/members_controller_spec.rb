require 'rails_helper'

RSpec.describe MembersController, type: :controller do
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @current_user = FactoryBot.create(:user)
    sign_in @current_user
    @campaign = create(:campaign, user: @current_user)
  end

  describe "POST #create" do
    before(:each) do
      request.env["HTTP_ACCEPT"] = 'application/json'
    end

    it "creates a new member in a campaign" do
      member = create(:member, campaign: @campaign)
      expect(member.campaign).to eql(@campaign)
      expect(@campaign.members.count).to eql(2)
    end

    it "returns success when creating a new member" do
      post :create, params: {member: { campaign_id: @campaign.id, email: 'teste@email.com', name: 'teste' }}
      expect(response).to have_http_status(:success)
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      request.env["HTTP_ACCEPT"] = 'application/json'
    end

    it "returns http success" do
      member = create(:member, campaign: @campaign)
      delete :destroy, params: {id: member.id}
      expect(response).to have_http_status(:success)
    end

    it "removes a member from a campaign when destroy the member" do
      member = create(:member, campaign: @campaign)
      expect(@campaign.members.count).to eql(2)
      delete :destroy, params: {id: member.id}
      expect(@campaign.members.count).to eql(1)
    end
  end

  describe "POST #update" do
    before(:each) do
      request.env["HTTP_ACCEPT"] = 'application/json'
      @member = create(:member, campaign: @campaign)
    end

    it "update a member name" do
      post :update, params: { id: @member.id, member: { name: 'teste nome' } }
      @member.reload
      expect(@member.name).to eql('teste nome')
    end

    it "update a member email" do
      post :update, params: { id: @member.id, member: { email: 'teste@email.com' } }
      @member.reload
      expect(@member.email).to eql('teste@email.com')
    end
  end
end
